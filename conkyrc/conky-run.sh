#!/bin/bash

# This script runs 2 conkyrc files at once

killall conky

bash -c "sleep 2; conky -c ~/.config/conkyrc/conkyrc1 >/dev/null 2>&1"

bash -c "sleep 3; conky -c ~/.config/conkyrc/conkyrc2 >/dev/null 2>&1"
